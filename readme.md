# Klassenarbeitsevaluation

Im Zuge der Veröffentlichung dieses Bogens in der PAEDAGOGIK, habe ich mich entschieden, den Bogen hier auf gitLab zur Verfügung zu stellen.

Mehr Informationen zu dem Bogen gibt es unter anderem hier: https://herrspitau.de/2016/06/22/der-evaluationsbogen-fuer-klassenarbeiten/

![Screenshot des Bogens](klassenarbeitsevaluation.jpg)
